# Base image comes from https://github.com/carlossg/docker-maven/blob/34e7d27260ee61c6866922c523e04b53ba098337/eclipse-temurin-17/Dockerfile
# which is maven:latest Dockerfile as of the time of writing this
FROM eclipse-temurin:17-jdk
WORKDIR /app
COPY ./target/main-0.0.1-SNAPSHOT.jar /app/app.jar
ENTRYPOINT [ "java", "-jar", "/app/app.jar" ]
