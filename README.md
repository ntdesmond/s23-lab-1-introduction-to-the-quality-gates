# Lab 1 -- Introduction to the quality gates

> By Vladislav Safonov, B19-SD-02

[![pipeline status](https://gitlab.com/ntdesmond/s23-lab-1-introduction-to-the-quality-gates/badges/main/pipeline.svg)](https://gitlab.com/ntdesmond/s23-lab-1-introduction-to-the-quality-gates/-/commits/main)

## Deployment details

CI builds and pushes docker image to [the project registry](https://gitlab.com/ntdesmond/s23-lab-1-introduction-to-the-quality-gates/container_registry).

Rest of the work is done on the target machine:

1. [`watchtower`](https://github.com/containrrr/watchtower/) is run to track for new image versions. It is configured to track only [labelled](https://containrrr.dev/watchtower/arguments/#filter_by_enable_label) containers. This approach allows to add more containers to track later without the need to restart `watchtower`.

    ```sh
    docker run -d \
        --name watchtower \
        -v /var/run/docker.sock:/var/run/docker.sock \
        containrrr/watchtower --label-enable --interval 300
    ```

2. The container itself is run with a label and a forwarded `8080` port. I used 20123 as the host machine port, whatever.

    ```sh
    docker run -d \
        --label=com.centurylinklabs.watchtower.enable=true \
        -p 20123:8080 \
        registry.gitlab.com/ntdesmond/s23-lab-1-introduction-to-the-quality-gates
    ```

## Successful deployment proofs

As my target machine is behind NAT, I can't make the endpoint reachable from the Internet. Instead, here are some screenshots:

1. `watchtower` logs that include update moment:

    ![watchtomer logs](https://i.imgur.com/TBcbbe1.png)

2. Testing the endpoint with `curl`:

    ![curl localhost:20123](https://i.imgur.com/9rSSGkP.png)
